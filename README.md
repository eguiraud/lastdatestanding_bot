# A telegram bot to find a good meeting date by excluding all others

Great for when you need a date when _everyone_ is available.

The bot keeps a separate calendar for each chat.
Members of the group can tap on dates to remove them from the calendar.
The last date standings are those when everyone is there!

<img src="demo.gif" width="500px">

## Get started

Just invite the bot to a Telegram group. A public instance is available as `@lastdatestanding_bot` (uptime is best effort).

Drop me a message at enrico.guiraud@pm.me if you find this useful!
