from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
from telegram import ParseMode, InlineKeyboardMarkup
from telegram import InlineKeyboardButton as Button
import logging
from calendar import Calendar, month_name 
from datetime import datetime
from redis import Redis
redis = Redis(db=0)

def get_redis_keys(chat_id, year, month, user="*"):
    return "{}:{}:{}:{}".format(chat_id, user, year, month)

def dummy_button(txt):
    return Button(txt, callback_data="dummy")

def day_button(year, month, day, deleted):
    _t = datetime.today()
    is_today = (_t.year, _t.month, _t.day) == (year, month, day)
    txt = ">{}<" if is_today else " {} "
    if day == 0 or deleted:
        txt = txt.format(" ")
        return dummy_button(txt)
    else:
        txt = txt.format(day)
        data = "{}-{}-{}".format(year, month, day)
        return Button(txt, callback_data=data)

def start(update, context):
    update.message.reply_text("Hello! Use /calendar to start killing off dates"\
                              "until the winner remains!", quote=False)

def display_month(update, context, year, month):
    # TODO no need to connect to redis if we know we are just deleting one day
    button_rows = [[dummy_button("{} {}".format(month_name[month], year))]]
    button_rows.append([dummy_button(s) for s in ("M","T","W","T","F","S","S")])

    chat_id = update.effective_chat.id
    keys = redis.keys(get_redis_keys(chat_id, year, month))
    bad_days = []
    for key in keys:
        bad_days.extend(map(int, redis.lrange(key, 0, -1)))

    month_days = Calendar().monthdayscalendar(year, month)
    for week in month_days:
        row = [day_button(year, month, day, deleted=day in bad_days) for day in week]
        button_rows.append(row)

    prev_month_month = 12 if month == 1 else month - 1
    prev_month_year = year - 1 if month == 1 else year
    next_month_month = 1 if month == 12 else month + 1
    next_month_year = year + 1 if month == 12 else year
    prev_key = "{}-{}".format(prev_month_year, prev_month_month)
    next_key = "{}-{}".format(next_month_year, next_month_month)
    button_rows.append([Button("Prev", callback_data=prev_key),
                        Button("Next", callback_data=next_key)])
    reset_key = "reset-{}-{}".format(year, month)
    show_key = "show-{}-{}".format(year, month)
    button_rows.append([Button("Show killers", callback_data=show_key),
                        Button("Undo my kills", callback_data=reset_key)])
    kb = InlineKeyboardMarkup(button_rows)

    q = update.callback_query
    if q is None:
        # must send first message
        update.message.reply_text("Kill the dates!!!", reply_markup=kb, quote=False)
    else:
        # is callback query, can edit message
        q.edit_message_reply_markup(kb)

def calendar(update, context):
    today = datetime.today()
    display_month(update, context, today.year, today.month)

def delete_day(update, context):
    q = update.callback_query
    year, month, day = (int(s) for s in q.data.split("-"))
    chat_id = update.effective_chat.id
    user = update.effective_user.username
    if user is None:
        user = update.effective_user.id
    key = get_redis_keys(chat_id, year, month, user)
    redis.rpush(key, day)
    display_month(update, context, year, month)

def change_month(update, context):
    # this should be `calendar` with a different month
    year, month = (int(s) for s in update.callback_query.data.split('-'))
    display_month(update, context, year, month)

def reset_month(update, context):
    year, month = (int(s) for s in update.callback_query.data.split('-')[1:])
    chat_id = update.effective_chat.id
    user = update.effective_user.username
    if user is None:
        user = update.effective_user.id
    keys = redis.keys(get_redis_keys(chat_id, year, month, user))
    if len(keys) == 0:
        update.callback_query.answer("Nothing to reset")
        return
    for key in keys:
        redis.delete(key)
    display_month(update, context, year, month)

def show_killers(update, context):
    q = update.callback_query
    year, month = map(int, q.data.split('-')[1:])
    chat_id = update.effective_chat.id
    keys = redis.keys(get_redis_keys(chat_id, year, month))
    if len(keys) == 0:
        update.callback_query.answer("No killers yet")
        return

    txt = "Future dates killed for {} {}\n".format(month_name[month], year)

    _today = datetime.today()
    def is_in_the_future(day):
        if _today.year == year and _today.month == month:
            return day >= _today.day
        elif _today.year == year:
            return month > _today.month
        else:
            return year > _today.year

    for key in keys:
        user = key.decode().split(':')[1]
        days_killed = map(int, redis.lrange(key, 0, -1))
        pretty_days_killed = sorted(filter(is_in_the_future, days_killed))
        if len(pretty_days_killed) > 0:
            txt += "*{}:* {}\n".format(user, pretty_days_killed)
    q.message.reply_text(txt, parse_mode=ParseMode.MARKDOWN, quote=False)

def handle_dummies(update, context):
    update.callback_query.answer("Tap on a day to kill it")

def run_bot():
    """Create logger, book bot handlers, start listening to messages."""
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=log_fmt, level=logging.INFO)

    updater = Updater(token='*****', use_context=True)
    d = updater.dispatcher
    d.add_handler(CommandHandler('start', start))
    d.add_handler(CommandHandler('calendar', calendar))
    d.add_handler(CallbackQueryHandler(delete_day, pattern="^[0-9]+-[0-9]+-[0-9]+$"))
    d.add_handler(CallbackQueryHandler(change_month, pattern="^[0-9]+-[0-9]+$"))
    d.add_handler(CallbackQueryHandler(reset_month, pattern="^reset-[0-9]+-[0-9]+$"))
    d.add_handler(CallbackQueryHandler(show_killers, pattern="^show-[0-9]+-[0-9]+$"))
    d.add_handler(CallbackQueryHandler(handle_dummies, pattern="^dummy$"))

    updater.start_polling()
    updater.idle()

if __name__ == "__main__":
    run_bot()
